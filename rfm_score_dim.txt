segment_name	score_category	R	F	M	C
ABOUT TO SLEEP	112	1	1	2	1
ABOUT TO SLEEP	113	1	1	3	1
ABOUT TO SLEEP	121	1	2	1	1
ABOUT TO SLEEP	122	1	2	2	1
ABOUT TO SLEEP	131	1	3	1	1
ABOUT TO SLEEP	211	2	1	1	1
ABOUT TO SLEEP	212	2	1	2	1
ABOUT TO SLEEP	221	2	2	1	1
RECENT CUSTOMER	311	3	1	1	2
RECENT CUSTOMER	312	3	1	2	2
RECENT CUSTOMER	313	3	1	3	2
CANNOT LOOSE THEM	123	1	2	3	3
CANNOT LOOSE THEM	132	1	3	2	3
CANNOT LOOSE THEM	133	1	3	3	3
CHAMPION	333	3	3	3	4
LOST CUSTOMER	111	1	1	1	5
LOYAL	233	2	3	3	6
LOYAL	323	3	2	3	6
LOYAL	332	3	3	2	6
POTENTIAL LOYALIST	223	2	2	3	7
POTENTIAL LOYALIST	232	2	3	2	7
POTENTIAL LOYALIST	322	3	2	2	7
POTENTIAL LOYALIST	331	3	3	1	7
AVERAGE	213	2	1	3	8
AVERAGE	222	2	2	2	8
AVERAGE	231	1	3	1	8
AVERAGE	321	3	2	1	8